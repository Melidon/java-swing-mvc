package swingmvclab;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

/*
 * A megjelenítendõ ablakunk osztálya.
 */
public class StudentFrame extends JFrame {

	/*
	 * Ebben az objektumban vannak a hallgatói adatok. A program indulás után
	 * betölti az adatokat fájlból, bezáráskor pedig kimenti oda.
	 * 
	 * NE MÓDOSÍTSD!
	 */
	private StudentData data;

	class StudentTableCellRenderer implements TableCellRenderer {
		private TableCellRenderer renderer;

		public StudentTableCellRenderer(TableCellRenderer defRenderer) {
			this.renderer = defRenderer;
		}

		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
				int row, int column) {
			Component component = renderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row,
					column);
			// Kikeressük az éppen megjelenítendő hallgatót a külső,
			// StudentFrame osztály data tagváltozójából,
			Student student = data.students.get(row);
			// megállapítjuk, hogy bukásra áll-e vagy sem,
			boolean heSucks = !student.hasSignature() || student.getGrade() < 2;
			// és ez alapján átállítjuk a komponens háttérszínét:
			if (heSucks) {
				// component.setBackground(...)
				component.setBackground(Color.RED);
			} else {
				component.setBackground(Color.GREEN);
			}
			return component;
		}
	}

	/*
	 * Itt hozzuk létre és adjuk hozzá az ablakunkhoz a különbözõ komponenseket
	 * (táblázat, beviteli mezõ, gomb).
	 */
	private void initComponents() {
		this.setLayout(new BorderLayout());
		JTable jt = new JTable();
		jt.setModel(this.data);
		jt.setFillsViewportHeight(true);
		jt.setRowSorter(new TableRowSorter<TableModel>(jt.getModel()));
		jt.setDefaultRenderer(String.class, new StudentTableCellRenderer(jt.getDefaultRenderer(String.class)));
		jt.setDefaultRenderer(Boolean.class, new StudentTableCellRenderer(jt.getDefaultRenderer(Boolean.class)));
		jt.setDefaultRenderer(Integer.class, new StudentTableCellRenderer(jt.getDefaultRenderer(Integer.class)));
		JScrollPane jsp = new JScrollPane(jt);
		this.add(jsp, BorderLayout.CENTER);
		JLabel name_jl = new JLabel("Név:");
		JTextField name_jtf = new JTextField(20);
		JLabel neptun_jl = new JLabel("Neptun:");
		JTextField neptun_jtf = new JTextField(6);
		JButton felvesz_jb = new JButton("Felvesz");
		felvesz_jb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				data.addStudent(name_jtf.getText(), neptun_jtf.getText());
			}
		});
		JPanel input = new JPanel();
		input.add(name_jl);
		input.add(name_jtf);
		input.add(neptun_jl);
		input.add(neptun_jtf);
		input.add(felvesz_jb);
		this.add(input, BorderLayout.SOUTH);

		this.pack();
	}

	/*
	 * Az ablak konstruktora.
	 * 
	 * NE MÓDOSÍTSD!
	 */
	@SuppressWarnings("unchecked")
	public StudentFrame() {
		super("Hallgatói nyilvántartás");
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		// Induláskor betöltjük az adatokat
		try {
			data = new StudentData();
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream("students.dat"));
			data.students = (List<Student>) ois.readObject();
			ois.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		// Bezáráskor mentjük az adatokat
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				try {
					ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("students.dat"));
					oos.writeObject(data.students);
					oos.close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		});

		// Felépítjük az ablakot
		setMinimumSize(new Dimension(500, 200));
		initComponents();
	}

	/*
	 * A program belépési pontja.
	 * 
	 * NE MÓDOSÍTSD!
	 */
	public static void main(String[] args) {
		// Megjelenítjük az ablakot
		StudentFrame sf = new StudentFrame();
		sf.setVisible(true);
	}
}
