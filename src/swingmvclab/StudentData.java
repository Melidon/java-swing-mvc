package swingmvclab;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

/*
 * A hallgat�k adatait t�rol� oszt�ly.
 */
public class StudentData extends AbstractTableModel {

	/*
	 * Ez a tagv�ltoz� t�rolja a hallgat�i adatokat. NE M�DOS�TSD!
	 */
	List<Student> students = new ArrayList<Student>();

	public void addStudent(String name, String neptun) {
		Student student = new Student(name, neptun, false, 0);
		this.students.add(student);
		this.fireTableRowsInserted(this.getRowCount() - 1, this.getRowCount() - 1);
	}

	@Override
	public int getRowCount() {
		return students.size();
	}

	@Override
	public int getColumnCount() {
		return 4;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Student student = students.get(rowIndex);
		switch (columnIndex) {
		case 0:
			return student.getName();
		case 1:
			return student.getNeptun();
		case 2:
			return student.hasSignature();
		default:
			return student.getGrade();
		}
	}

	@Override
	public String getColumnName(int column) {
		switch (column) {
		case 0:
			return "N�v";
		case 1:
			return "Neptun";
		case 2:
			return "Al��r�s";
		default:
			return "Jegy";
		}
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
		case 0:
			return String.class;
		case 1:
			return String.class;
		case 2:
			return Boolean.class;
		default:
			return Integer.class;
		}
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return columnIndex >= 2;
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		Student element = students.get(rowIndex);
		switch (columnIndex) {
		case 0:
			element.setName((String) aValue);
			break;
		case 1:
			element.setNeptun((String) aValue);
			break;
		case 2:
			element.setSignature((Boolean) aValue);
			break;
		default:
			element.setGrade((Integer) aValue);
			break;
		}
		this.fireTableRowsUpdated(rowIndex, rowIndex);
	}

}
